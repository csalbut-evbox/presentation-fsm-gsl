/*
 * FSM.c
 *
 * Brief: Finite State Machine (FSM) engine.
 *
 * Copyright (C) EVBox B.V., All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
#include "FSM.h"
#include <stdio.h>
#include "Assert.h"


/* Private types
 * ---------------------------------------------------------------------------------------------- */
struct FSM {
    uint32_t id;
    const char * name;
    const FSM_State *all_states;
    const FSM_State *current_state;
    int pending_event;
    void *user_data;
};

/* Private data
 * ---------------------------------------------------------------------------------------------- */
static struct FSM fsm_pool[FSM_MAX_COUNT];
static uint32_t num_fsms_in_use = 0;

/* Private functions
 * ---------------------------------------------------------------------------------------------- */
static void fsm_try_entry_action(const FSM *fsm)
{
    if (fsm->current_state->on_entry != NULL) {
        fsm->current_state->on_entry(fsm->user_data);
    }
}

static void fsm_try_exit_action(const FSM *fsm)
{
    if (fsm->current_state->on_exit != NULL) {
        fsm->current_state->on_exit(fsm->user_data);
    }
}

static void fsm_try_tick_action(const FSM *fsm)
{
    if (fsm->current_state->on_tick != NULL) {
        fsm->current_state->on_tick(fsm->user_data);
    }
}

static void fsm_try_transition(FSM *fsm)
{
    if (fsm->pending_event != 0) {

        int next_state_id = fsm->current_state->transitions[fsm->pending_event];

        fsm->pending_event = 0;

        if (next_state_id != 0) {
            fsm_try_exit_action(fsm);
            fsm->current_state = &fsm->all_states[next_state_id];
            fsm_try_entry_action(fsm);
        }
    }
}

/* Public functions
 * ---------------------------------------------------------------------------------------------- */
// TODO start_state belongs in the xml model
FSM *FSM_New(const char *name, const FSM_State *state_table, const FSM_State *start_state,
                    void *user_data)
{
    ASSERT(state_table != NULL);
    ASSERT(start_state != NULL);

    uint32_t new_fsm_idx = num_fsms_in_use;
    struct FSM *new_fsm = &fsm_pool[new_fsm_idx];
    ASSERT(new_fsm != NULL);
    num_fsms_in_use++;
    ASSERT(num_fsms_in_use <= FSM_MAX_COUNT);

    new_fsm->id = new_fsm_idx;
    new_fsm->name = name;
    new_fsm->all_states = state_table;
    new_fsm->current_state = start_state;
    new_fsm->pending_event = 0;

    if (user_data != NULL) {
        new_fsm->user_data = user_data;
    } else {
        new_fsm->user_data = (void*)new_fsm;
    }

    return new_fsm;
}

void FSM_Tick(FSM *fsm)
{
    ASSERT(fsm);
    fsm_try_tick_action(fsm);
    fsm_try_transition(fsm);
}

void FSM_SendEvent(FSM *fsm, const int event)
{
    fsm->pending_event = event;
}

int FSM_GetCurrentState(const FSM *fsm)
{
    return fsm->current_state->id;
}

void FSM_InitEntryAction(FSM *fsm)
{
    fsm_try_entry_action((const FSM*)fsm);
}

