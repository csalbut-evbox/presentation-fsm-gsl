/*
 * FSM.h
 *
 * Brief: Finite State Machine (FSM) engine.
 *
 * Copyright (C) EVBox B.V., All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

#ifndef FSM_H_
#define FSM_H_

/* Includes
 * ---------------------------------------------------------------------------------------------- */
#include <stdint.h>
#include <stdbool.h>

/* Defines
 * ---------------------------------------------------------------------------------------------- */
// Number of state machines in the system (size of the pool)
#define FSM_MAX_COUNT 10

// Max number of events that can be defined for an FSM instance.
#define MAX_EVENT_COUNT     10

/* Public types
 * ---------------------------------------------------------------------------------------------- */
typedef struct FSM_State {
    int id;
    void (*on_entry)(void *data);
    void (*on_tick)(void *data);
    void (*on_exit)(void *data);
    int transitions[MAX_EVENT_COUNT];
} FSM_State;

typedef struct FSM FSM;

/* Public functions
 * ---------------------------------------------------------------------------------------------- */
FSM* FSM_New (const char *name, const FSM_State *state_table, const FSM_State *start_state,
                    void *user_data);

void FSM_Tick (FSM *fsm);
void FSM_SendEvent (FSM *fsm, const int event);
int FSM_GetCurrentState(const FSM *fsm);
void FSM_InitEntryAction(FSM *fsm);

#endif // FSM_H_
