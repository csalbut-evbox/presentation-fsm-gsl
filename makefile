things := relay processor seasons

DIAGRAM_UMLS := $(addprefix fsm_, $(addsuffix .uml, $(things)))
DIAGRAMS := $(addprefix fsm_, $(addsuffix .png, $(things)))
FSM_MD_TABLES := $(addprefix fsm_table_, $(addsuffix .md, $(things)))
FSM_HTML_TABLES := $(addprefix fsm_table_, $(addsuffix .html, $(things)))

.PHONY: help
help:
	@echo "Try the following make targets:"
	@echo "  * slides   -- builds the presentation html slides"
	@echo "  * diagrams -- generates plantuml diagrams and .png images based on them"
	@echo "  * tables   -- FSM lookup tables, the markdown way"
	@echo ""
	@echo "Also, look into the makefile to see how this stuff is built."

.PHONY: slides
slides: presentation.html

.PHONY: diagrams
diagrams: $(DIAGRAMS)

.PHONY: tables
tables: $(FSM_MD_TABLES) $(FSM_HTML_TABLES)

presentation.html: presentation.md theme/css/screen.css $(DIAGRAMS)
	@landslide --embed -l table -x tables $<

fsm_table_%.md: models/fsm_%.xml md_table.gsl
	gsl -q -script:md_table.gsl $< > $@

fsm_table_%.html: models/fsm_%.xml fsm_html_tables.gsl
	gsl -q -script:fsm_html_tables.gsl $< > $@

fsm_%.uml: models/fsm_%.xml fsm_diagram.gsl
	@gsl -q -script:fsm_diagram.gsl $< > $@

fsm_%.png: fsm_%.uml
	@plantuml $<

PRECIOUS: $(DIAGRAM_UMLS)

debug:
	@echo "diagrams: $(DIAGRAMS)"

clean:
	rm -f presentation.html $(DIAGRAM_UMLS) $(DIAGRAMS) $(FSM_MD_TABLES) $(FSM_HTML_TABLES)
