# FSM & GSL

---

# Disclaimer: Biases of the speaker

This biases are carried over to the presented FSM implementation

- bare-metal, hard real time applications
- low latencies
- only static memory allocation
- custom-cutting software to fit the application needs
- resolving as much logic as possible at compilation time
	- logic at runtime is expensive
	- runtime memory is expensive


---

# FSM engine

- No name, just two files: `FSM.c`, `FSM.h`
- Targeted at bare-metal embedded software
- Code generation optional
- Totally fine to write FSMs manually
	- but boring after a couple of repetitions
	- and easy to make copy-paste errors

### Presenter notes

generation bonus: easier to spot bugs in a graph with circles and arrows<br>

---

# It's tiny (100 LOC)

	$ cloc Src/FSM.c Inc/FSM.h
	------------------------------------------------------
	Language        files      blank    comment       code
	------------------------------------------------------
	C                   1         24         22         87
	C/C++ Header        1          9         19         21
	------------------------------------------------------
	SUM:                2         33         41        108
	------------------------------------------------------

---
# What problems does it solve?

- Fewer bugs
- Never end up in a wrong/corrupted state
- Never make an unexpected/invalid transition
- Dealing with shared state is hard,
    - especially when the state has form of several loose variables.
	- this approach makes it easier

- Very convenient design pattern for event-driven MCU software

### Presenter notes

Elvi (G4) source code<br>
    Ad-hoc FSM (sequential), one big switch-case, very brittle<br>
Peace of mind. Switch-case / if-else: need to be super aware when modifying<br>
Frees brainpower to solve other problemss<br>

---
# What problems does it solve?

## Bonus: lightweight non-blocking concurrency

- Doesn't matter if you have an OS
- Still, OS and FSMs can live fine together
- Multiple state machines can run concurrently

        !c
        while(true) {
            FSM_Tick(fsm_app);
            FSM_Tick(fsm_hmi);
            FSM_Tick(fsm_calibration);
        }

- They can be nested (FSM running inside a single state of another FSM).

---
# What types of FSMs are supported?

"When the machine is in state *S*, and event *E* happens, the machine makes the transition to a different state"

![data processor FSM diagram](img/fsm_processor.svg)


### Presenter notes
Moore/Mealy?<br>
	Moore, (almost)<br>
	Moore "inputs" -- "events"<br>
	Moore "outputs" -- we don't have "outputs"<br>


---
# Action Hooks

- Actions are associated with states, not with transitions

![action hooks](img/fsm_action_hooks.svg)

### Presenter notes

A  little different than the OCPP approach.<br>

---
# State Transition Table

| state      | e:null | e:data available | e:processing done |
| --------   | ------ | ----------       | ----------        |
| Waiting    | -      | processing       | -                 |
| Processing | -      | -                | waiting           |

---
# How it works

- The "engine" + transition lookup table (LUT)
- The engine, in each tick:
	- looks at the current state
	- looks at events received
	- looks at the LUT
	- if the event should cause a transition, does the transition
	- calls appropriate "action hooks" (callbacks)


---
# Interface

    !c
    FSM* FSM_New (const char *name,
                    const FSM_State *state_table,
                    const FSM_State *start_state,
                    void *user_data);

    void FSM_Tick (FSM *fsm);
    void FSM_SendEvent (FSM *fsm, const int event);
    int FSM_GetCurrentState(const FSM *fsm);
    void FSM_InitEntryAction(FSM *fsm);

---
# Interface

Abstract Data Type: `FSM`

    !c
    typedef struct FSM FSM;

Just like `FILE` from  `<stdio.h>`

    !c
	/* The opaque type of streams. */
	typedef struct _IO_FILE FILE;

## Google-fu hints:

- "Abstract Data Type". Look for examples of stacks and queues.
- "Incomplete Type", "Opaque Type" -- specific terms for C language.

---
# LUT Source Code

    !c
    static const FSM_State Proc_tx_table [] = {
        [PROC_STATE_WAITING] = {
            .id = PROC_STATE_WAITING,
            .transitions = {
                [PROC_EVENT_DATA AVAILABLE] = PROC_STATE_PROCESSING,
            },
            .on_entry = ProcWaitingOnEntry,
            .on_exit  = ProcWaitingOnExit,
            .on_tick  = ProcWaitingOnTick,
        },
        [PROC_STATE_PROCESSING] = {
            .id = PROC_STATE_PROCESSING,
            .transitions = {
                [PROC_EVENT_PROCESSING DONE] = PROC_STATE_WAITING,
            },
            .on_entry = ProcProcessingOnEntry,
            .on_exit  = ProcProcessingOnExit,
            .on_tick  = ProcProcessingOnTick,
        },
    };

---
# Default Action Handlers

    !c
    void WEAK ProcWaitingOnEntry (void *data) {
        (void)data;
    }

    void WEAK ProcWaitingOnExit (void *data) {
        (void)data;
    }

    void WEAK ProcWaitingOnTick (void *data) {
        (void)data;
    }

    void WEAK ProcProcessingOnEntry (void *data) {
        (void)data;
    }

    void WEAK ProcProcessingOnExit (void *data) {
        (void)data;
    }

    void WEAK ProcProcessingOnTick (void *data) {
        (void)data;
    }

---
# How to implement an FSM?

1. Draw it on paper. It's important. Really, do it.
2. Encode the diagram in the state transition LUT
3. Spawn the FSM:
    - `my_fsm = FSM_New("name", lut, initial_state, &user_data)`
4. Run it:  `FSM_Tick(my_fsm)`
5. Send events to it: `FSM_SendEvent(my_fsm, event)`
6. Implement `on_entry, on_exit, on_tick` actions, as needed.

---
# Main Application Loop

    !c
    main {
        initialize_everything();

        while(true) {
            FSM_Tick(fsm_app);
            FSM_Tick(fsm_comm);
            FSM_Tick(fsm_hmi);
            FSM_Tick(fsm_charge_manager);
            FSM_Tick(fsm_relay1);
            FSM_Tick(fsm_relay2);
        }
    }

### Presenter notes

concurrency!<br>
problem: FSMs must be nice to each other and not block.<br>
no such problem on RTOS -- scheduler preempts blocking tasks.<br>

---

# Problems, Deficiencies, Tradeoffs

- Very inefficient use of memory
- Static allocation
- No way to delete FSMs and free memory
- No event queueing

---
# Watch out for: spamming events

    !c
    /* Bad. If we tick the FSM multiple times per day, it will change
     * the season multiple  times on that day. */
    if (day_of_year == "march-21") {
        FSM_SendEvent(fsm_seasons, SEASONS_EVENT_NEXT);
    }

    /* Better */
    int state = FSM_GetCurrentState(fsm_seasons);
    if ((day_of_year == "march-21") && (state == SEASONS_STATE_WINTER)) {
        FSM_SendEvent(fsm_seasons, SEASONS_EVENT_NEXT);
    }

This is pretty much the only source of bugs when working with this engine.

Well this, and forgetting to send an event.

---
# `(void*) user_data` - what is it for?

    !c
    FSM* FSM_New (const char *name,
                    const FSM_State *state_table,
                    const FSM_State *start_state,
                    void *user_data);


`void RelayOpeningOnEntry (void *data)`

- Our "actions" cannot take custom parameters, or return values.
- So they are limited to communicating via shared global/static variables.
- Let's encapsulate necessary data into one object and pass it around.
- We can avoid the mess of multiple shared static variables.


---
# `(void*) user_data` - what is it for?

    !c
    struct Relay {
        int id;
        L9733 *driverChip;
        FSM *fsm; // Let's keep the FSM as just one part of the object.
        unsigned int driverOutputNum;
        unsigned int driverVddNum;
        RelayErr error;
    };

    static Relay* Relay_Init(int id)
    {
        struct Relay *newRelay = &relayPool[id];
        newRelay->id = id;
        newRelay->error = RELAY_NO_ERROR;
        newRelay->fsm = FSM_Relay_New((void*)newRelay);
        ...

---

# Should we use it everywhere?

- I don't know
- Different applications - different requirements and constraints.
- I can't guarantee this FSM engine is good for all use cases
- Never been properly stress-tested
    - I don't know when it breaks

### Presenter notes

We have different _classes_ of apps (bare-metal, linux, networked)<br>
My experience is limited to a handful of cases.<br>
Andries: the HMI state table. Would it work?<br>


---

# GSL

Name: "The General Schema Language", or "The Generator Script Language".

- FOSS, maintained by ZeroMQ
	- <https://github.com/zeromq/gsl>

- CLI tool
	- like a compiler: files in -> files out
	- nice to automate & integrate with CI

- Like a templating engine (Jade, Jinja, HAML)
	- not limited to the type of produced outputs
    - also has a non-template mode (scripting mode)

---
# GSL data flow

<br>

![GSL data flow](img/gsl_data_flow.svg)

<br>
<br>
<br>

## Invocation

    !shell
    $ gsl -script:fsm_code_template.gsl fsm_relay.xml  > fsm_relay.c

### Presenter notes

"Model" like in MVC pattern. Anyone familiar?<br>
model must be XML. Simple schema (tags & attributes)<br>
set of tags & attributes is up to us<br>

---
# GSL: what does it offer?

- Parses and loads the model to memory
- Allows us to find, iterate, do logic on elements
- Allows us to format and output text with the data from the model mixed in.



        !xml
        <fsm name     = "Data Processor"
          abbr        = "proc"
          first_state = "waiting"
          description = "Basic example of a data processing FSM.
                          For educational purposes only!" >

          <states>
            <state name = "waiting">
              <transition event = "data available" new_state = "processing" />
            </state>

            <state name = "processing">
              <transition event = "processing done" new_state = "waiting" />
            </state>
          </states>

          <events>
            <event name = "data available" />
            <event name = "processing done" />
          </events>
        </fsm>

### Presenter notes
It's like jquery<br>

---
# GSL: simple template example

File: `fsm_template.txt`

    My state machine is named "$(name:upper)"

    It has the following states:
    .for fsm->states
        - $(name)
    .endfor

    It reacts to the following events:
    .for fsm->events
        - $(name)
    .endfor

---
# GSL: simple template example

    $ gsl -q -script:fsm_template.txt models/fsm_processor.xml

    My state machine is named "DATA PROCESSOR"

    It has the following states:
            - waiting
            - processing

    It reacts to the following events:
            - data available
            - processing done

---
# GSL problems and downsides

- Custom syntax
- Documentation is not great
- Mixing two languages in one file
	- This is a universal code generation problem

- Scripts can be hard to write
    - trial and error
    - templates are quite easy

- Open question: are there known better tools?

---
# Code generation: silver bullet?

- No

- Nice: we can have a single source of truth (the model)

- Works well when the model data is fairly static
    - state machines
    - protocols

- Nice: saves us from repetition (when writing code, documents, diagrams)

- Otherwise, repetition is handled just fine by computers at runtime
    - loops, anyone?

### Presenter notes

SSOT: not limited to one programming language. We make various artifacts (code, docs, images)
from the same model.<br>

Repetition at runtime can be expensive<br>


---

# Backup slides

---
# OCPP example

![alt text](img/ocpp_fsm_example.jpg)

## Presenter notes

ocpp "conditions" -- my "events"<br>
ocpp "actions" -- my "actions"<br>

---
# FSM example: relay FSM

![seasons FSM diagram](fsm_relay.png)

---
# FSM example: seasons

![seasons FSM diagram](fsm_seasons.png)

---

# Other approaches to FSM

- <https://github.com/QuantumLeaps>
- <https://www.w3.org/TR/scxml/> -- W3C specification for FSMs
- <https://en.wikipedia.org/wiki/SCXML>
- <https://scion.scxml.io/>
- <https://github.com/tklab-tud/uscxml>
- <https://github.com/Phrogz/LXSC> - SCXML interpreter in Lua

---
# Tools needed to play with this presentation

- Landslide - turns a markdown file into HTML slides
    - `pip install landslide`
- GSL
    - <https://github.com/zeromq/gsl#building-and-installing>
- PlantUML
    - <http://plantuml.com/>
- Make

---
# Where to get stuff

- GSL compiled for windows (cygwin) on our Box
    - <https://evbox.box.com/s/9ad1c2arl4rf30cvg0rmwidl0m3r40pq>
- GSL source (github)
	- <https://github.com/zeromq/gsl>
- FSM engine: Power-MCU or Safety-MCU
    - also attached here, `./engine/FSM.{c,h}`


---

# Abbreviations

- FSM: Finite State Machine
- FOSS: Free Open Source Software
- CLI: Command Line Interface
- OS, RTOS: (Real-Time) Operating System
- MCU: Microcontroller (Unit)
- LUT: Lookup Table
- KSS: Knowledge Sharing Session

